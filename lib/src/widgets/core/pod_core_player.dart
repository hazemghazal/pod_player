part of 'package:pod_player/src/pod_player.dart';

class _PodCoreVideoPlayer extends StatefulWidget {
  final VideoPlayerController videoPlayerCtr;
  final double videoAspectRatio;
  final String tag;

  const _PodCoreVideoPlayer({
    Key? key,
    required this.videoPlayerCtr,
    required this.videoAspectRatio,
    required this.tag,
  }) : super(key: key);

  @override
  State<_PodCoreVideoPlayer> createState() => _PodCoreVideoPlayerState();
}

class _PodCoreVideoPlayerState extends State<_PodCoreVideoPlayer>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  bool first = true;
  bool second = false;
  bool third = false;
  @override
  Widget build(BuildContext context) {
    final _podCtr = Get.find<PodGetXVideoController>(tag: widget.tag);
    return Builder(
      builder: (_ctrx) {
        return RawKeyboardListener(
          autofocus: true,
          focusNode:
              (_podCtr.isFullScreen ? FocusNode() : _podCtr.keyboardFocusWeb) ??
                  FocusNode(),
          onKey: (value) => _podCtr.onKeyBoardEvents(
            event: value,
            appContext: _ctrx,
            tag: widget.tag,
          ),
          child: Stack(
            fit: StackFit.expand,
            children: [
              Center(
                child: AspectRatio(
                  aspectRatio: widget.videoAspectRatio,
                  child: VideoPlayer(widget.videoPlayerCtr),
                ),
              ),
              Visibility(
                visible: (_podCtr.waterMark != null && first),
                child: Positioned(
                  right: 0,
                  child: AnimatedBuilder(
                    animation: _animationController,
                    builder: (context, child) {
                      return Transform.translate(
                        offset: Offset(
                          MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? -MediaQuery.of(context).size.width /
                                  1.3 *
                                  _animationController.value
                              : -MediaQuery.of(context).size.width *
                                  _animationController.value,
                          MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? _animationController.value *
                                  MediaQuery.of(context).size.height /
                                  4.2
                              : _animationController.value *
                                  MediaQuery.of(context).size.height,
                        ),
                        child: child,
                      );
                    },
                    child: Container(
                      color: Colors.black.withOpacity(0.6),
                      child: Text(
                        _podCtr.waterMark ?? '',
                        style: TextStyle(
                          color: Colors.white.withOpacity(0.5),
                          fontFeatures: const [
                            FontFeature.proportionalFigures()
                          ],
                          fontSize: 18 - 0,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: (_podCtr.waterMark != null && second),
                child: Positioned(
                  left: -2,
                  child: AnimatedBuilder(
                    animation: _animationController,
                    builder: (context, child) {
                      return Transform.translate(
                        offset: Offset(
                          MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? MediaQuery.of(context).size.width /
                                  1.3 *
                                  _animationController.value
                              : MediaQuery.of(context).size.width *
                                  _animationController.value,
                          MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? _animationController.value *
                                  MediaQuery.of(context).size.height /
                                  4.2
                              : _animationController.value *
                                  MediaQuery.of(context).size.height,
                        ),
                        child: child,
                      );
                    },
                    child: Container(
                      color: Colors.black.withOpacity(0.6),
                      child: Text(
                        _podCtr.waterMark ?? '',
                        style: TextStyle(
                          color: Colors.white.withOpacity(0.5),
                          fontFeatures: const [
                            FontFeature.proportionalFigures()
                          ],
                          fontSize: 18 - 0,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Visibility(
                visible: (_podCtr.waterMark != null && third),
                child: Positioned(
                  right: 150,
                  child: AnimatedBuilder(
                    animation: _animationController,
                    builder: (context, child) {
                      return Transform.translate(
                        offset: Offset(
                          MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? _animationController.value
                              : _animationController.value -
                                  MediaQuery.of(context).size.width / 3.7,
                          MediaQuery.of(context).orientation ==
                                  Orientation.portrait
                              ? _animationController.value *
                                  MediaQuery.of(context).size.height /
                                  4.2
                              : MediaQuery.of(context).size.height *
                                  _animationController.value,
                        ),
                        child: child,
                      );
                    },
                    child: Container(
                      color: Colors.black.withOpacity(0.6),
                      child: Text(
                        _podCtr.waterMark ?? '',
                        style: TextStyle(
                          color: Colors.white.withOpacity(0.5),
                          fontFeatures: const [
                            FontFeature.proportionalFigures()
                          ],
                          fontSize: 18 - 0,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              GetBuilder<PodGetXVideoController>(
                tag: widget.tag,
                id: 'podVideoState',
                builder: (_) => GetBuilder<PodGetXVideoController>(
                  tag: widget.tag,
                  id: 'video-progress',
                  builder: (_podCtr) {
                    if (_podCtr.videoThumbnail == null) {
                      return const SizedBox();
                    }

                    if (_podCtr.podVideoState == PodVideoState.paused &&
                        _podCtr.videoPosition == Duration.zero) {
                      return SizedBox.expand(
                        child: TweenAnimationBuilder<double>(
                          builder: (context, value, child) => Opacity(
                            opacity: value,
                            child: child,
                          ),
                          tween: Tween<double>(begin: 0.7, end: 1),
                          duration: const Duration(milliseconds: 400),
                          child: DecoratedBox(
                            decoration: BoxDecoration(
                              image: _podCtr.videoThumbnail,
                            ),
                          ),
                        ),
                      );
                    }
                    return const SizedBox();
                  },
                ),
              ),
              _VideoOverlays(tag: widget.tag),
              IgnorePointer(
                child: GetBuilder<PodGetXVideoController>(
                  tag: widget.tag,
                  id: 'podVideoState',
                  builder: (_podCtr) {
                    final loadingWidget = _podCtr.onLoading?.call(context) ??
                        const Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Colors.transparent,
                            color: Colors.white,
                            strokeWidth: 2,
                          ),
                        );

                    if (kIsWeb) {
                      switch (_podCtr.podVideoState) {
                        case PodVideoState.loading:
                          return loadingWidget;
                        case PodVideoState.paused:
                          return const Center(
                            child: Icon(
                              Icons.play_arrow,
                              size: 45,
                              color: Colors.white,
                            ),
                          );
                        case PodVideoState.playing:
                          return Center(
                            child: TweenAnimationBuilder<double>(
                              builder: (context, value, child) => Opacity(
                                opacity: value,
                                child: child,
                              ),
                              tween: Tween<double>(begin: 1, end: 0),
                              duration: const Duration(seconds: 1),
                              child: const Icon(
                                Icons.pause,
                                size: 45,
                                color: Colors.white,
                              ),
                            ),
                          );
                        case PodVideoState.error:
                          return const SizedBox();
                      }
                    } else {
                      if (_podCtr.podVideoState == PodVideoState.loading) {
                        return loadingWidget;
                      }
                      return const SizedBox();
                    }
                  },
                ),
              ),
              if (!kIsWeb)
                GetBuilder<PodGetXVideoController>(
                  tag: widget.tag,
                  id: 'full-screen',
                  builder: (_podCtr) => _podCtr.isFullScreen
                      ? const SizedBox()
                      : GetBuilder<PodGetXVideoController>(
                          tag: widget.tag,
                          id: 'overlay',
                          builder: (_podCtr) => _podCtr.isOverlayVisible ||
                                  !_podCtr.alwaysShowProgressBar
                              ? const SizedBox()
                              : Align(
                                  alignment: Alignment.bottomCenter,
                                  child: PodProgressBar(
                                    tag: widget.tag,
                                    alignment: Alignment.bottomCenter,
                                    podProgressBarConfig:
                                        _podCtr.podProgressBarConfig,
                                  ),
                                ),
                        ),
                ),
            ],
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _animationController
      ..stop()
      ..dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 7),
    );

    _animationController
      ..animateTo(5)
      ..addListener(() {
        if (_animationController.value.isEqual(1.0)) {
          if (_animationController.value.isEqual(1.0)) {
            if (first) {
              second = true;
              first = false;
              setState(() {});
            } else if (second) {
              third = true;
              second = false;
              setState(() {});
            } else if (third) {
              first = true;
              third = false;
              setState(() {});
            }
          }
          _animationController
            ..reset()
            ..animateBack(5);
        }
      });
  }
}
